<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '77de74c42ffbd5a010fbd3bb18bce0552fb86b4b',
        'name' => 'rizify/increase-billing-legacy',
        'dev' => true,
    ),
    'versions' => array(
        'rizify/increase-billing-legacy' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '77de74c42ffbd5a010fbd3bb18bce0552fb86b4b',
            'dev_requirement' => false,
        ),
    ),
);
