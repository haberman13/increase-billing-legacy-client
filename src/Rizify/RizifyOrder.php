<?php

namespace Rizify;

class RizifyOrder
{
    public $cc_cvv = null;
    public $cc_yr = null;
    public $cc_mo = null;
    public $cc_number = null;
    public $gateway_id = null;
    public $mid_id = null;
    public $price = null;
    public $tax = null;
    public $shipping = null;
    public $id = null;
    public $transaction_id = null;
    public $vault_id = null;
    public $amount = null;
    public $description = null;
    public $redirect = null;
    public $products = null;
    public $tax_rate = null;
    public $retry_reference_id = null;
    public $order_id = null;
    public $subscription_id = null;
    public $product_name = null;
    public $product_id = null;
    public $payment_token = null;
    public $return_url = null;
    
    public $a = null;
    public $s1 = null;
    public $s2 = null;
    public $s3 = null;
    public $campaign_id = null;
    public $campaign_name = null;
    public $adset_id = null;
    public $adset_name = null;
    public $utm_campaign = null;
    public $utm_adgroup = null;
    public $utm_ad = null;    
    public $mobile = null;
    public $device = null;
    public $vault_add = null;
    public $card_add = null;

    public function __construct($data)
    {
        if (empty($data))
        {
            throw new \Exception("Missing order information");
        }

        foreach ($data as $prop => $d)
        {
            if (property_exists($this, $prop))
            {
                $this->{$prop} = $d;
            }
            else
            {
                throw new \Exception("This property does not exist on a SmartlyOrder object");
            }
        }
    }    
}
